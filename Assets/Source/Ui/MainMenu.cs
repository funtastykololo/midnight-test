using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _play;
    [SerializeField] private Button _exit;
    [SerializeField] private TextMeshProUGUI _winCount;
    [SerializeField] private TextMeshProUGUI _loseCount;

    private DataContainer _data;

    private void Awake()
    {
        _play.onClick.AddListener(PlayButton);
        _exit.onClick.AddListener(ExitButton);
    }

    private void Start()
    {
        _data = Data.Instance.Load();
        _winCount.text = _data.winCount.ToString();
        _loseCount.text = _data.loseCount.ToString();
    }

    private void PlayButton()
    {
        SceneManager.LoadScene("Template");
    }

    private void ExitButton()
    {
        Application.Quit();
    }
}
