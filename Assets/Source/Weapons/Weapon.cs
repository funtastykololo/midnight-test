using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected float _range;
    [SerializeField] protected float _damage;
    [SerializeField] protected int _ammoCount;
    [SerializeField] protected LayerMask _layer;
    [SerializeField] protected ParticleSystem _fx;
    [SerializeField] protected Transform _muzzle;
    [SerializeField] protected LineRenderer _lineRenderer;
    [SerializeField] protected float _lineDelay;

    public abstract int AmmoCount { get; set; }
    public abstract WeaponType Type { get; set; }
    public abstract void Shooting(bool fire, Camera camera);
    public abstract void Activate(bool state);

    protected Vector3[] _linePos;
    protected WaitForSeconds _lineRenderDelay;
    protected RaycastHit _hit;

    public enum WeaponType
    {
        Pistol,
        Rifle,
        Shotgun
    }
}
