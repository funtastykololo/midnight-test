using System;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public event Action OnPlayerWin;

    public static LevelManager Instance;

    private int _enemyCount;
    private int _enemyDeathCount;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;

        ResumeGame();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
    }

    public void EnemyDeathCounter()
    {
        _enemyDeathCount++;

        if (_enemyDeathCount == _enemyCount)
        {
            OnPlayerWin?.Invoke();
        }
    }
}
