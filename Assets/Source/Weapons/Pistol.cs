using System.Collections;
using UnityEngine;

public class Pistol : Weapon
{
    public override WeaponType Type { get; set; }

    private void Awake()
    {
        Type = WeaponType.Pistol;

        _lineRenderDelay = new(_lineDelay);
        _linePos = new Vector3[_lineRenderer.positionCount];
        _linePos[0] = _muzzle.position;
    }

    public override int AmmoCount 
    { 
        get => _ammoCount; 
        set => _ammoCount = value; 
    }

    public override void Activate(bool state)
    {
        gameObject.SetActive(state);
    }

    public override void Shooting(bool fire, Camera camera)
    {
        if (_ammoCount > 0 && fire)
            RaycastShoot(camera);
    }

    private void RaycastShoot(Camera camera)
    {
        _ammoCount--;
        _fx.Play();

        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out _hit, _range, _layer))
        {
            if (_hit.transform.TryGetComponent<Enemy>(out Enemy enemy))
            {
                StartCoroutine(ProjectileRoutine(_hit.point));
                enemy.TakeDamage(_damage);
            }
        }
    }

    private IEnumerator ProjectileRoutine(Vector3 hit)
    {
        _lineRenderer.enabled = true;
        _linePos[0] = _muzzle.position;
        _linePos[1] = hit;
        _lineRenderer.SetPositions(_linePos);
        yield return _lineRenderDelay;
        _lineRenderer.enabled = false;
    }
}
