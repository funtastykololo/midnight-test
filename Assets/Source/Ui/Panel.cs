using UnityEngine;
using UnityEngine.SceneManagement;

public class Panel : MonoBehaviour
{
    protected DataContainer _data;

    private string _currentScene;

    protected virtual void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        _currentScene = SceneManager.GetActiveScene().name;

        _data = Data.Instance.Load();

        LevelManager.Instance.PauseGame();
    }

    protected void Restart()
    {
        SceneManager.LoadScene(_currentScene);
    }

    protected void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }
}
