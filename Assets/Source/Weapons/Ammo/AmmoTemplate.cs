using UnityEngine;

[CreateAssetMenu]
public class AmmoTemplate : ScriptableObject
{
    [SerializeField] private AmmoTypes _ammo;
    [SerializeField] private int _ammoCount;

    public AmmoTypes AmmoType => _ammo;
    public int AmmoCount => _ammoCount;

    public enum AmmoTypes
    {
        Pistol,
        Rifle,
        Shotgun
    }
}
