using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private InputPlayer _input;
    [SerializeField] private float _camRotSpeed = 1f;
    [SerializeField] private Vector2 _verticalRotConstraint;
    [SerializeField] private Transform _rotationRoot;
    [SerializeField] private Vector3 _aimingOffset;
    [SerializeField] private CinemachineCameraOffset _camOffset;
    [SerializeField] private RectTransform _crosshair;

    private Vector3 _rotation;
    private Vector3 _constraintRotation;
    private float _angle;

    private void Awake()
    {
        _rotationRoot.eulerAngles = Vector3.zero;
    }

    private void OnEnable()
    {
        _input.OnPlayerAiming += HandleOnPlayerAiming;
    }

    private void OnDisable()
    {
        _input.OnPlayerAiming -= HandleOnPlayerAiming;
    }

    private void Update()
    {
        Rotation();
    }

    private void HandleOnPlayerAiming(bool isAiming)
    {
        if (isAiming)
        {
            _camOffset.m_Offset = _aimingOffset;
            _crosshair.gameObject.SetActive(true);
        }
        else
        {
            _camOffset.m_Offset = Vector3.zero;
            _crosshair.gameObject.SetActive(false);
        }
    }

    private void Rotation()
    {
        if (_input.Rotation != Vector2.zero)
        {
            _rotation = new Vector3(_input.Rotation.y, 0f, 0f) * _camRotSpeed * Time.deltaTime;
            _rotationRoot.Rotate(_rotation);
        }

        _constraintRotation = _rotationRoot.transform.localEulerAngles;
        _angle = _rotationRoot.transform.localEulerAngles.x;

        if (_angle > 180 && _angle < _verticalRotConstraint.y)
        {
            _constraintRotation.x = _verticalRotConstraint.y;
            _rotationRoot.transform.localEulerAngles = _constraintRotation;
        }
        else if (_angle < 180 && _angle > _verticalRotConstraint.x)
        {
            _constraintRotation.x = _verticalRotConstraint.x;
            _rotationRoot.transform.localEulerAngles = _constraintRotation;
        }
    }
}
