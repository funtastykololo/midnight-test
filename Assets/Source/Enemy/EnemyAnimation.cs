using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Enemy _enemy;
    [SerializeField] private CapsuleCollider _collider;

    private float _radiusCollider;

    private void Awake()
    {
        _radiusCollider = _collider.radius;
    }

    private void OnEnable()
    {
        _enemy.OnEnemyAttack += HandleOnEnemyAttack;
    }

    private void OnDisable()
    {
        _enemy.OnEnemyAttack -= HandleOnEnemyAttack;
    }

    private void Update()
    {
        _animator.SetBool("Move", _enemy.Move);
    }

    private void HandleOnEnemyAttack()
    {
        _animator.Play("Attack");
    }

    private void BeginAttack()
    {
        _collider.enabled = true;
    }

    private void EndAttack()
    {
        _collider.enabled = false;
    }
}
