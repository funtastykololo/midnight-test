using TMPro;
using UnityEngine;

public class PlayUi : MonoBehaviour
{
    [SerializeField] private PlayerController _player;
    [SerializeField] private LevelManager _lvlManager;
    [SerializeField] private WeaponHandler _weapon;
    [SerializeField] private TextMeshProUGUI _ammoCount;
    [SerializeField] private LosePanel _losePanel;
    [SerializeField] private WinPanel _winPanel;

    private void OnEnable()
    {
        _player.OnPlayerDie += HandleOnPlayerDie;
        _lvlManager.OnPlayerWin += HandleOnPlayerWin;
    }

    private void OnDisable()
    {
        _player.OnPlayerDie -= HandleOnPlayerDie;
        _lvlManager.OnPlayerWin -= HandleOnPlayerWin;
    }

    private void Update()
    {
        _ammoCount.text = _weapon.CurrentWeapon.AmmoCount.ToString();
    }

    private void HandleOnPlayerWin()
    {
        _winPanel.gameObject.SetActive(true);
    }

    private void HandleOnPlayerDie()
    {
        _losePanel.gameObject.SetActive(true);
    }
}
