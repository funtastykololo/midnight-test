using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputPlayer : MonoBehaviour
{
    public event Action OnPlayerJump;
    public event Action OnPlayerShoot;
    public event Action OnNextWeapon;
    public event Action OnPreviousWeapon;
    public event Action<bool> OnPlayerAiming;

    [SerializeField] private PlayerInput _input;

    public Vector2 Direction { get; private set; }
    public Vector2 Rotation { get; private set; }
    public bool IsAiming { get; private set; }
    public bool IsShooting { get; private set; }
    public float RunMultiplier { get; private set; }

    private InputAction _move;
    private InputAction _rotation;

    private void Awake()
    {
        _move = _input.actions["Move"];
        _rotation = _input.actions["Look"];
        RunMultiplier = 1f;
    }

    private void OnEnable()
    {
        _input.actions["Run"].performed += HandleOnRunPress;
        _input.actions["Jump"].performed += HandleOnJumpPress;
        _input.actions["Aiming"].performed += HandleOnRightClick;
        _input.actions["Fire"].performed += HandleOnLeftClick;
        _input.actions["NextWeapon"].performed += HandleOnNextWeaponPress;
        _input.actions["PreviousWeapon"].performed += HandleOnPreviousWeaponPress;
    }

    private void OnDisable()
    {
        _input.actions["Run"].performed -= HandleOnRunPress;
        _input.actions["Jump"].performed -= HandleOnJumpPress;
        _input.actions["Aiming"].performed -= HandleOnRightClick;
        _input.actions["Fire"].performed -= HandleOnLeftClick;
        _input.actions["NextWeapon"].performed -= HandleOnNextWeaponPress;
        _input.actions["PreviousWeapon"].performed -= HandleOnPreviousWeaponPress;
    }

    private void Update()
    {
        Direction = _move.ReadValue<Vector2>();
        Rotation = _rotation.ReadValue<Vector2>();
    }

    private void HandleOnJumpPress(InputAction.CallbackContext context)
    {
        if (!IsAiming)
        OnPlayerJump?.Invoke();
    }

    private void HandleOnRunPress(InputAction.CallbackContext context)
    {
        if (IsAiming)
        {
            RunMultiplier = 1f;
            return;
        }
        RunMultiplier = context.ReadValue<float>();
    }

    private void HandleOnLeftClick(InputAction.CallbackContext context)
    {
        IsShooting = _input.actions["Fire"].IsPressed();

        if (IsAiming)
        {
            OnPlayerShoot?.Invoke();
        }
    }

    private void HandleOnRightClick(InputAction.CallbackContext context)
    {
        IsAiming = _input.actions["Aiming"].IsPressed();
        OnPlayerAiming?.Invoke(IsAiming);
    }

    private void HandleOnNextWeaponPress(InputAction.CallbackContext context)
    {
        OnNextWeapon?.Invoke();
    }

    private void HandleOnPreviousWeaponPress(InputAction.CallbackContext context)
    {
        OnPreviousWeapon?.Invoke();
    }
}
