using UnityEngine;

public class WeaponHandler : MonoBehaviour
{
    [SerializeField] private InputPlayer _input;
    [SerializeField] private Camera _camera;
    [SerializeField] private Inventory _inventory;

    public Weapon CurrentWeapon => _currentWeapon;

    private Weapon _currentWeapon;

    private int _weaponCurrentIndex;

    private void Awake()
    {
        _currentWeapon = _inventory.Weapons[_weaponCurrentIndex];
        _currentWeapon.Activate(true);
    }

    private void OnEnable()
    {
        _input.OnPlayerShoot += HandleOnlayerShoot;
        _input.OnNextWeapon += HandleOnNextWeapon;
        _input.OnPreviousWeapon += HandleOnPreviousWeapon;
    }

    private void OnDisable()
    {
        _input.OnPlayerShoot -= HandleOnlayerShoot;
        _input.OnNextWeapon -= HandleOnNextWeapon;
        _input.OnPreviousWeapon -= HandleOnPreviousWeapon;
    }

    private void HandleOnlayerShoot()
    {
        _currentWeapon.Shooting(_input.IsShooting, _camera);
    }

    private void HandleOnNextWeapon()
    {
        _currentWeapon.Activate(false);
        _weaponCurrentIndex++;

        if (_weaponCurrentIndex > _inventory.Weapons.Length - 1)
        {
            _weaponCurrentIndex = _inventory.Weapons.Length - 1;
        }

        _currentWeapon = _inventory.Weapons[_weaponCurrentIndex];
        _currentWeapon.Activate(true);
    }

    private void HandleOnPreviousWeapon()
    {
        _currentWeapon.Activate(false);
        _weaponCurrentIndex--;

        if (_weaponCurrentIndex < 0)
        {
            _weaponCurrentIndex = 0;
        }

        _currentWeapon = _inventory.Weapons[_weaponCurrentIndex];
        _currentWeapon.Activate(true);
    }
}
