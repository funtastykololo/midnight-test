using System.Collections;
using UnityEngine;

public class Rifle : Weapon
{
    [SerializeField] private float _shootCooldown;
    public override WeaponType Type { get ; set; }

    public override int AmmoCount
    {
        get => _ammoCount;
        set => _ammoCount = value;
    }

    private Coroutine _shootingRoutine;

    private void Awake()
    {
        Type = WeaponType.Rifle;

        _lineRenderDelay = new(_lineDelay);
        _linePos = new Vector3[_lineRenderer.positionCount];
        _linePos[0] = _muzzle.position;
    }

    public override void Activate(bool state)
    {
        gameObject.SetActive(state);
    }

    public override void Shooting(bool fire, Camera camera)
    {
        if (_shootingRoutine != null)
            StopCoroutine(_shootingRoutine);
        if (!fire)
            return;

        _shootingRoutine = StartCoroutine(ShootingRoutine(camera));
    }

    private IEnumerator ShootingRoutine(Camera camera)
    {
        while(_ammoCount > 0)
        {
            RaycastShoot(camera);
            yield return new WaitForSecondsRealtime(_shootCooldown);
        }
    }

    private void RaycastShoot(Camera camera)
    {
        _ammoCount--;
        _fx.Play();

        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out _hit, _range, _layer))
        {
            if (_hit.transform.TryGetComponent<Enemy>(out Enemy enemy))
            {
                StartCoroutine(ProjectileRoutine(_hit.point));
                enemy.TakeDamage(_damage);
            }
        }
    }

    private IEnumerator ProjectileRoutine(Vector3 hit)
    {
        _lineRenderer.enabled = true;
        _linePos[0] = _muzzle.position;
        _linePos[1] = hit;
        _lineRenderer.SetPositions(_linePos);
        yield return _lineRenderDelay;
        _lineRenderer.enabled = false;
    }
}
