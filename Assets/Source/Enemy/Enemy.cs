using System;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public event Action OnEnemyAttack;

    [SerializeField] private float _hp = 1f;
    [SerializeField] private float _damage = 1f;
    [SerializeField] private float _attackCooldown = 1f;
    [SerializeField] private float _closestDistance = 1f;
    [SerializeField] private float _detectDistance = 10f;
    [SerializeField] private NavMeshAgent _enemy;

    public float Damage => _damage;
    public bool Move { get; private set; }

    private Transform _player;
    private Vector3 _distance;
    private float _sqrDistance;
    private float _currentTimeAttack;
    private float _timeLastAttack;

    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        CheckDistance();

        if (_sqrDistance <= _closestDistance * _closestDistance)
        {
            transform.LookAt(_player.transform);

            _enemy.isStopped = true;
            Move = false;

            _currentTimeAttack = Time.time - _timeLastAttack;
            if (_currentTimeAttack > _attackCooldown)
            {
                OnEnemyAttack?.Invoke();
                _timeLastAttack = Time.time;
            }
            return;
        }

        if (_sqrDistance <= _detectDistance * _detectDistance)
        {
            _enemy.isStopped = false;
            _enemy.destination = _player.position;
            Move = true;
        }

    }

    public void TakeDamage(float damage)
    {
        _hp -= damage;

        if (_hp <= 0f)
        {
            LevelManager.Instance.EnemyDeathCounter();
            gameObject.SetActive(false);
        }
    }

    private void CheckDistance()
    {
        _distance = transform.position - _player.position;
        _sqrDistance = Vector3.SqrMagnitude(_distance);
    }
}
