using UnityEngine;

public class PlayerLocomotion : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private float _rotationSpeed = 1f;
    [SerializeField] private float _jumpHeight = 1f;
    [SerializeField] private float _jumpForce = 1f;
    [SerializeField] private float _fallForce = -1f;
    [SerializeField] private InputPlayer _input;
    [SerializeField] private CharacterController _charController;

    private Vector3 _inputValue;
    private Vector3 _moveDirection;
    private Vector3 _targetRotation;
    private float _gravity;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        _gravity = _fallForce;
    }

    private void OnEnable()
    {
        _input.OnPlayerJump += HandleOnPlayerJump;
    }

    private void OnDisable()
    {
        _input.OnPlayerJump -= HandleOnPlayerJump;
    }

    private void Update()
    {
        Move();
        Rotation();

        GroundCheck();
    }

    private void HandleOnPlayerJump()
    {
        if (_charController.isGrounded)
        {
            _gravity = _jumpForce;
        }
    }

    private void Move()
    {
        _inputValue = new Vector3(_input.Direction.x, _gravity, _input.Direction.y) * _moveSpeed * _input.RunMultiplier * Time.deltaTime;
        _moveDirection = transform.right * _inputValue.x + transform.up * _inputValue.y + transform.forward * _inputValue.z;
        _charController.Move(_moveDirection);
    }

    private void Rotation()
    {
        _targetRotation = new Vector3(0f, _input.Rotation.x, 0f) * _rotationSpeed * Time.deltaTime;
        _charController.transform.Rotate(_targetRotation);
    }

    private void GroundCheck()
    {
        if (_charController.transform.position.y > _jumpHeight)
        {
            _gravity = _fallForce;
        }
    }
}
