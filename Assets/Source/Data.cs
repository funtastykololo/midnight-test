using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using UnityEngine;

public class Data : MonoBehaviour
{
    public static Data Instance;

    private BinaryFormatter _binaryFormatter;
    private FileStream _fileStream;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        if (!File.Exists(Application.persistentDataPath + "/SavedData.dat"))
        {
            SetDefaultData();
        }
    }

    public void Save(DataContainer data)
    {
        _binaryFormatter = new BinaryFormatter();

        if (File.Exists(Application.persistentDataPath + "/SavedData.dat"))
        {
            _fileStream = File.Open(Application.persistentDataPath + "/SavedData.dat", FileMode.Open);
        }
        else
            Debug.Log("File does not exist");

        _binaryFormatter.Serialize(_fileStream, data);
        _fileStream.Close();
    }

    public DataContainer Load()
    {
        _binaryFormatter = new BinaryFormatter();

        if (File.Exists(Application.persistentDataPath + "/SavedData.dat"))
        {
            _fileStream = File.Open(Application.persistentDataPath + "/SavedData.dat", FileMode.Open);
        }
        else
            Debug.Log("File does not exist");

        DataContainer data = (DataContainer)_binaryFormatter.Deserialize(_fileStream);
        _fileStream.Close();

        return data;
    }

    private void SetDefaultData()
    {
        _fileStream = File.Create(Application.persistentDataPath + "/SavedData.dat");
        _binaryFormatter = new BinaryFormatter();

        DataContainer data = new();
        data.winCount = 0;
        data.loseCount = 0;

        _binaryFormatter.Serialize(_fileStream, data);
        _fileStream.Close();
    }
}

[Serializable]
public class DataContainer
{
    public int winCount;
    public int loseCount;
}

