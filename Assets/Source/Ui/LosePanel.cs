using UnityEngine;
using UnityEngine.UI;

public class LosePanel : Panel
{
    [SerializeField] private Button _restart;
    [SerializeField] private Button _mainMenu;

    protected override void Awake()
    {
        base.Awake();
        _restart.onClick.AddListener(Restart);
        _mainMenu.onClick.AddListener(MainMenu);
    }

    private void OnEnable()
    {
        Debug.Log("Lose");
        _data.loseCount++;
        Data.Instance.Save(_data);
    }
}

