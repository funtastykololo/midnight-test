using UnityEngine;
using UnityEngine.UI;

public class WinPanel : Panel
{
    [SerializeField] private Button _restart;
    [SerializeField] private Button _mainMenu;

    protected override void Awake()
    {
        base.Awake();
        _restart.onClick.AddListener(Restart);
        _mainMenu.onClick.AddListener(MainMenu);
    }

    private void OnEnable()
    {
        Debug.Log("Win");
        _data.winCount++;
        Data.Instance.Save(_data);
    }
}
