using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Weapon[] _weapons;

    public Weapon[] Weapons => _weapons;
}
