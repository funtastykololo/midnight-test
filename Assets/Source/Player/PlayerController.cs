using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public event Action OnPlayerDie;

    [SerializeField] private float _hp = 10f;
    [SerializeField] private Inventory _inventory;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
        {
            AmmoTemplate ammo = (AmmoTemplate)item.AmmoType;

            switch (ammo.AmmoType)
            {
                case AmmoTemplate.AmmoTypes.Pistol:
                    _inventory.Weapons[0].AmmoCount += ammo.AmmoCount;
                    break;
                case AmmoTemplate.AmmoTypes.Rifle:
                    _inventory.Weapons[1].AmmoCount += ammo.AmmoCount;
                    break;
                case AmmoTemplate.AmmoTypes.Shotgun:
                    _inventory.Weapons[2].AmmoCount += ammo.AmmoCount;
                    break;
            }
            item.gameObject.SetActive(false);
        }
    }

    public void TakeDamage(float damage)
    {
        _hp -= damage;

        if (_hp <= 0f)
        {
            OnPlayerDie?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
