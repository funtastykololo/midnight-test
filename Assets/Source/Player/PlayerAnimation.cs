using UnityEngine;
using UnityEngine.Animations.Rigging;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Transform _aim;
    [SerializeField] private Camera _camera;
    [SerializeField] private Animator _animator;
    [SerializeField] private WeaponHandler _weaponHandler;
    [SerializeField] private InputPlayer _input;
    [SerializeField] private Rig _weaponsAim;
    [SerializeField] private Rig _pistolIK;
    [SerializeField] private Rig _rifleIK;
    [SerializeField] private Rig _shotgunIK;

    private Ray _ray;

    private void OnEnable()
    {
        _input.OnPlayerJump += HandleOnPlayerJump;
    }

    private void OnDisable()
    {
        _input.OnPlayerJump -= HandleOnPlayerJump;
    }

    private void Update()
    {
        if (_input.Direction != Vector2.zero)
        {
            _animator.SetBool("Run", true);
        }
        else
            _animator.SetBool("Run", false);

        if (_input.IsAiming)
        {
            _weaponsAim.weight = 1f;
            _animator.SetBool("Aim", true);
            _animator.SetLayerWeight(1, 1f);

            switch (_weaponHandler.CurrentWeapon.Type)
            {
                case Weapon.WeaponType.Pistol:
                    _pistolIK.weight = 1f;
                    _rifleIK.weight = 0f;
                    _shotgunIK.weight = 0f;
                    break;
                case Weapon.WeaponType.Rifle:
                    _rifleIK.weight = 1f;
                    _pistolIK.weight = 0f;
                    _shotgunIK.weight = 0f;
                    break;
                case Weapon.WeaponType.Shotgun:
                    _shotgunIK.weight = 1f;
                    _rifleIK.weight = 0f;
                    _pistolIK.weight = 0f;
                    break;
            }
        }
        else
        {
            _animator.SetBool("Aim", false);
            _animator.SetLayerWeight(1, 0f);
            _weaponsAim.weight = 0f;
            _pistolIK.weight = 0f;
            _rifleIK.weight = 0f;
            _shotgunIK.weight = 0f;
        }

        _animator.SetFloat("RunMultiplier", _input.RunMultiplier);

        _ray = _camera.ScreenPointToRay(Input.mousePosition);
        _aim.position = _ray.direction + new Vector3(0f, 1f, 0f) + transform.position;

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawRay(_camera.transform.position, _camera.transform.forward * 100);
    }

    private void HandleOnPlayerJump()
    {
        _animator.SetTrigger("Jump");
    }
}
