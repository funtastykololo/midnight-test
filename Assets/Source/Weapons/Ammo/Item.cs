using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private ScriptableObject _ammoType;
    [SerializeField] private MeshRenderer _pistolModel;
    [SerializeField] private MeshRenderer _rifleModel;
    [SerializeField] private MeshRenderer _shotgunModel;

    public ScriptableObject AmmoType => _ammoType;

    private AmmoTemplate ammo;

    private void Awake()
    {
        ammo = (AmmoTemplate)AmmoType;

        switch (ammo.AmmoType)
        {
            case AmmoTemplate.AmmoTypes.Pistol:
                _pistolModel.gameObject.SetActive(true);
                break;
            case AmmoTemplate.AmmoTypes.Rifle:
                _rifleModel.gameObject.SetActive(true);
                break;
            case AmmoTemplate.AmmoTypes.Shotgun:
                _shotgunModel.gameObject.SetActive(true);
                break;
        }
    }
}
